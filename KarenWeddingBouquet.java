public class KarenWeddingBouquet extends Bouquet {

    public KarenWeddingBouquet() {
        name = "Regular Wedding Bouquet";
        colorScheme.add("White");
        colorScheme.add("Light Blue");
    }
}
