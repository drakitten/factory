public class Main {

    public static void main(String[] args) {
        BouquetShop gothShop = new GothBouquetShop();
        BouquetShop karenShop = new KarenBouquetShop();

        Bouquet bouquet = karenShop.orderBouquet("easter");
        System.out.println("");
        bouquet = gothShop.orderBouquet("wedding");
    }
}
