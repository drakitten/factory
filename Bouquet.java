import java.util.ArrayList;

public abstract class Bouquet {

    protected String name;
    protected ArrayList<String> colorScheme;

    public Bouquet() {
        this.colorScheme = new ArrayList<String>();
    }

    public void arrange() {
        System.out.println("Getting fresh plants. In the color scheme: ");
        for (String color: colorScheme) {
            System.out.println(color);
        }
        System.out.println("Arranging them aesthetically.");
    }

    public void wrap() {
        System.out.println("Wrapping plants.");
    }

    public void ship() {
        System.out.println("Shipping plants to you.");
    }

    public String getName() {
        return name;
    }
}
