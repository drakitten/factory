public class KarenEasterBouquet extends Bouquet {

    public KarenEasterBouquet() {
        name = "Easter Bouquet";
        colorScheme.add("Red");
        colorScheme.add("Yellow");
        colorScheme.add("Orange");
    }
}
