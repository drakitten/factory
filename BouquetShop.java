public abstract class BouquetShop {

    protected abstract Bouquet requestBouquet(String type);

    public Bouquet orderBouquet(String type) {

        Bouquet bouquet = requestBouquet(type);

        bouquet.arrange();
        bouquet.wrap();
        bouquet.ship();

        return bouquet;
    }
}
