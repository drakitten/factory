public class GothBouquetShop extends BouquetShop {

    public Bouquet requestBouquet(String type) {
        if (type.equals("wedding")) return new GothWeddingBouquet();
        else if (type.equals("easter")) return new GothEasterBouquet();
        else if (type.equals("nosegay")) return new GothNosegayBouquet();
        return null;
    }
}
