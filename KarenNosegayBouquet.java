public class KarenNosegayBouquet extends Bouquet {

    public KarenNosegayBouquet() {
        name = "Nosegay bouquet";
        colorScheme.add("Orange");
        colorScheme.add("Pink");
    }

    @Override
    public void wrap() {
        System.out.println("Making the cutest bouquet ~reee ");
    }
}
