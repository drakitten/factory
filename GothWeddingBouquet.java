public class GothWeddingBouquet extends Bouquet {

    public GothWeddingBouquet() {
        name = "Goth Wedding Bouquet";
        colorScheme.add("Black");
    }

    @Override
    public void wrap() {
        System.out.println("Wrapping bouquet as in Corpse Bride.");
    }
}
