public class KarenBouquetShop extends BouquetShop {

    public Bouquet requestBouquet(String type) {
        if (type == "wedding") return new KarenWeddingBouquet();
        else if (type.equals("easter")) return new KarenEasterBouquet();
        else if (type.equals("nosegay")) return new KarenNosegayBouquet();
        return null;

    }
}
