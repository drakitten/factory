public class GothNosegayBouquet extends Bouquet {

    public GothNosegayBouquet() {
        name = "Goth Nosegay Bouquet";
        colorScheme.add("Red");
        colorScheme.add("Black");
    }
}
