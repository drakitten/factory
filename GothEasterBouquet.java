import java.util.ArrayList;

public class GothEasterBouquet extends Bouquet {

    public GothEasterBouquet() {
        name = "Goth Easter Bouquet";
        colorScheme.add("Black");
        colorScheme.add("Gray");
    }

    @Override
    public void wrap() {
        System.out.println("Wrapping by adding black eggs.");
    }
}
